# Application Performance and Memory Management

This repo based on [Udemy](https://www.udemy.com/course/java-application-performance-and-memory-management) course about JVM application perfomance. Also, here you can find [link](http://1.droppdf.com/files/iTkkV/oreilly-java-performance-the-definitive-guide-2014.pdf) to book.

### How the JVM runs your code
Let's talk about [compilation process](https://www.oreilly.com/library/view/java-performance-2nd/9781492056102/ch04.html). 

1. Java is designed to take advantage of the platform-independence of scripting languages and the native performance of compiled languages.
2. A Java class file is compiled into an intermediate language (Java bytecodes) that are then further compiled into assembly language by the JVM.
3. Compilation of the byte codes into assembly language performs a number of optimizations that greatly improve performance.

```
Java file -> [javac] -> class file
```

Class file - is bytecode for JVM, which next is interpreted to machine code. But general thing, that JIT compiler also works in runtime throught all this process.

Just-In-Time ([JIT](https://aboullaite.me/understanding-jit-compiler-just-in-time-compiler/)) compiler is a component of the Java Runtime Environment that improves the performance of Java applications at run time. 

<a href="https://ibb.co/mqxvyCs"><img src="https://i.ibb.co/T8CK0BX/image.png" alt="image" border="0"></a>

How JIT optimize execution of bytecode? Actually, JIT analyze which bytecode instructions are used more frequently (loops, frequently-called method) and just in runtime compile them to native machine code. After that, JIT just reuse this piece of native code. 

<a href="https://ibb.co/GHT097d"><img src="https://i.ibb.co/2ZNySqF/image.png" alt="image" border="0"></a>

#### Example

Here you can see that 3, 4, 5 lines was compiled to native code in runtime by JIT compiler.

<a href="https://ibb.co/p0f54bB"><img src="https://i.ibb.co/JBvgrmw/image.png" alt="image" border="0"></a>

### More about JIT compiler
As we already know, if code instruction usage will be frequently repeat, JIT will put this decompiled instruction to cache. 

The more java program works, the less jit compiler takes effect.
<a href="https://ibb.co/4spBwbd"><img src="https://i.ibb.co/tZDyw0X/image.png" alt="image" border="0"></a>

Let's print compilation dump of JIT. Just use [next VM options](https://www.oracle.com/technetwork/java/javase/tech/vmoptions-jsp-140102.html): PrintCompilation

If PrintCompilation is enabled, every time a method (or loop) is compiled, the JVM prints out a line with information about what has just been compiled. The output of that has varied somewhat between Java releases; the output discussed here became standardized in Java 7.

```
java -XX:+PrintCompilation $MAIN_CLASS_HERE$ args
```

We will get next result:
```
...
    178  190       3       java.lang.Integer::<init> (10 bytes)
    178  192 %     4       PrimeNumbers::isPrime @ 2 (35 bytes)
    178  191       1       java.util.ArrayList::size (5 bytes)
    180  193       3       java.util.ArrayList::add (25 bytes)
    180  195       3       PrimeNumbers::getNextPrimeAbove (43 bytes)
    181  194       3       java.util.ArrayList::add (23 bytes)
    183  195       3       PrimeNumbers::getNextPrimeAbove (43 bytes)   made not entrant
    183  196       3       PrimeNumbers::getNextPrimeAbove (43 bytes)
    183  197       4       PrimeNumbers::isPrime (35 bytes)
    185  198       1       java.lang.invoke.MethodType::ptypes (5 bytes)
    185  199     n 0       java.lang.invoke.MethodHandle::linkToStatic(L)L (native)   (static)
...    
```

_First column_ - time (number of milliseconds) since Virtual Machine started, _second column_ - order in which method or code block was compiled, _third column_ - [flags](https://blog.joda.org/2011/08/printcompilation-jvm-flag.html), _fourth column_ - native level (0 - 4), _fifth column_ - line of code, actually compiled.

Most lines of the compilation log have the following format:
```
timestamp | compilation_id | attributes (tiered_level) | method_name | size | deopt
```

1. __timestamp__: time after the compilation has finished (relative to 0, which is when the JVM started).
2. __compilation_id__: internal task ID. Usually, this number will simply increase monotonically, but sometimes you may see an out-of-order compilation ID. This happens most frequently when there are multiple compilation threads (JVM works asynchronous) and indicates that compilation threads are running faster or slower relative to each other. Don’t conclude, though, that one particular compilation task was somehow inordinately slow: it is usually just a function of thread scheduling.
3. __attributes__:  series of five characters that indicates the state of the code being compiled. If a particular attribute applies to the given compilation, the character shown in the following list is printed; otherwise, a space is printed for that attribute. Hence, the five-character attribute string may appear as two or more items separated by spaces. 

<a href="https://ibb.co/ykfmYh7"><img src="https://i.ibb.co/LgkDJQH/image.png" alt="image" border="0"></a>

The first of these attributes refers to on-stack replacement (OSR). JIT compilation is an asynchronous process: when the JVM decides that a certain method should be compiled, that method is placed in a queue. Rather than wait for the compilation, the JVM then continues interpreting the method, and the next time the method is called, the JVM will execute the compiled version of the method (assuming the compilation has finished, of course).

But consider a long-running loop. The JVM will notice that the loop itself should be compiled and will queue that code for compilation. But that isn’t sufficient: the JVM has to have the ability to start executing the compiled version of the loop while the loop is still running—it would be inefficient to wait until the loop and enclosing method exit (which may not even happen). Hence, when the code for the loop has finished compiling, the JVM replaces the code (on stack), and the next iteration of the loop will execute the much faster compiled version of the code. This is OSR.

#### Tiered Compilation Levels
Actually, there are two standart types of  JIT compilers: `C1` and `C2`. And `5` types of levels (`0` level not on the picture).

<a href="https://ibb.co/ZJLcbhw"><img src="https://i.ibb.co/thmY1bN/image.png" alt="image" border="0"></a>

<a href="https://ibb.co/b381Fwn"><img src="https://i.ibb.co/PYfxMX8/image.png" alt="image" border="0"></a>

### Server & Client compilers
Client - `C1`, Server - `C2`.

With Tiered compilation, the Client compiler is used at the beginning to make startup fast, then, when the code becomes hot and profile data is collected, it is recompiled by the Server compiler.

The usual path is `0 -> 3 -> 4`, so the code is interpreted first, then, when it gets hot enough, it’s compiled by `C1` with full profiling enabled and, finally, `C2` compiles the code using profile data collected by `C1`. But there are 3 exceptions:
1. __The Method Is Simple__: If the method to be compiled is trivial, then it is compiled only at level 1 (C1 without profiling) because the Server compiler wouldn’t make it faster – there is no point in doing extensive profiling to find out how the code is used if it can be made fast without it
2. __C2 Is Busy__: If at some point the Server compiler queue gets full, the method will be taken from the Server queue and compiled at level 2. After some time, the code will be compiled at level 3 (with full profiling) and finally, when the Server queue is less busy, it will be compiled by the Server compiler.
3. __C1 Is Busy, but C2 Is Not__: If the C1 queue is full, but the C2 queue is not, then the method can be profiled by the interpreter (level 0) and then it can go directly to C2. 

__Finilize__:
During the process of compiletsion, all methods starts with 0 level via `C1`. But in application lifecycle, another level of compilation can be choosen (according on method/instruction usage).

### Code cache of JIT
The Java Virtual Machine (JVM) generates native code and stores it in a memory area called the [codecache](https://docs.oracle.com/javase/8/embedded/develop-apps-platforms/codecache.htm#A1101612). Not only JIT use this codecache, but:
>The JIT is by far the biggest user of the codecache.

Keep in mind that the codecache starts relatively small and then grows as needed as new methods are compiled. Sometimes compiled methods are freed from the codecache, especially when the maximum size of the codecache is constrained. The memory used by free methods can be reused for newly compiled methods, allowing additional methods to be compiled without growing the codecache further.

You can get information on codecache usage by specifying `–XX:+PrintCodeCache` on the java launcher command line. When your application exits, you will see output similar to the following:

```
CodeCache: size=32768Kb used=542Kb max_used=542Kb free=32226Kb
 bounds [0xb414a000, 0xb41d2000, 0xb614a000] 
 total_blobs=131 nmethods=5 adapters=63 
 compilation: enabled
```

__size__: The maximum size of the codecache. It should be equivalent to what was specified by –XX:ReservedCodeCacheSize. Note that this is not the actual amount of physical memory (RAM) used by the codecache. This is just the amount of virtual address space set aside for it.

__used__: The amount of codecache memory actually in use. This is usually the amount of RAM the codecache occupies. However, due to fragmentation and the intermixing of free and allocated blocks of memory within the codecache, it is possible that the codecache occupies more RAM than is indicated by this value, because blocks that were used then freed are likely still in RAM.

__max_used__: This is the high water mark for codecache usage; the maximum size that the codecache has grown to and used. This generally is considered to be the amount of RAM occupied by the codecache, and will include any free memory in the codecache that was at some point in use. For this reason, it is the number you will likely want to use when determining how much codecache your application is using.

__free__: This is size minus used.

### Bytecode generation lifecycle
Here we can see typical lifecycle of application life.

<a href="https://ibb.co/pnb5SsP"><img src="https://i.ibb.co/M9ZQmjh/image.png" alt="image" border="0"></a>

Here it is, JIT compiler optimize some instructions. JIT starts with interpretation of code, then profile it and do optimization (dynamic compilation). What does it means? To optimize application, VM decides to compile native code with some extra changes.

<a href="https://ibb.co/7z5dPf5"><img src="https://i.ibb.co/Sx84bj8/image.png" alt="image" border="0"></a>

As example:
```
void f(boolean flag1, boolean flag2) {
    if (flag1) {
        ...
    } else {
        ...
    }
}
```
If profiling tells JIT, that flag1 - always true, then code can be reduced in next way:
```
void f(boolean flag1, boolean flag2) {
    if (flag1) {
        ...
    }
}
```
So, JIT optimize code and generates native code with `C2` cpmpiler. But what if someone calls `f()` function with `flag2 = true`? Then optimization is wrong and JIT need somehow to decompile previous one native code. It's known as __deoptimization__.

### Deoptimization
Deoptimization means that the compiler has to “undo” a previous compilation. Deoptimization occurs in two cases: when code is __made not entrant__ and when code is __made zombie__. 

<a href="https://ibb.co/cDYRkPG"><img src="https://i.ibb.co/LnNBYML/image.png" alt="image" border="0"></a>

#### Not entrant code
Code can be made not entrant in the following scenarios:
1. During Tiered Compilation.
2. When using a polymorphism.

#### Tiered Compilation
Due to the nature of [Tiered Compilation](https://docs.oracle.com/javacomponents/jrockit-hotspot/migration-guide/comp-opt.htm#JRHMG119), when the method is compiled using different levels of compilation, the code compiled using the previous level is made not entrant. What it basically means is that the compilation on a new level is made from scratch (however, it can use profile data that's already been collected).

>Tiered compilation, introduced in Java SE 7, brings client startup speeds to the server VM. A server VM uses the interpreter to collect profiling information about methods that is fed into the compiler. In the tiered scheme, in addition to the interpreter, the client compiler generates compiled versions of methods that collect profiling information about themselves. Since the compiled code is substantially faster than the interpreter, the program executes with greater performance during this profiling phase. In many cases, a startup that is even faster than with the client VM can be achieved because the final code produced by the server compiler may be already available during the early stages of application initialization. The tiered scheme can also achieve better peak performance than a regular server VM because the faster profiling phase allows a longer period of profiling, which may yield better optimization. Use the -XX:+TieredCompilation flag with the java command to enable tiered compilation.

Also, you can dissable [tiered compilation](https://stackoverflow.com/questions/38721235/what-exactly-does-xx-tieredcompilation-do).    
> In Java SE 8, Tiered compilation is the default mode for the server VM. Both 32 and 64 bit modes are supported. -XX:-TieredCompilation flag can be used to disable tiered compilation

#### Using a polymorphism
```
Parser parser;
if (document.isDocumentLong()) {
  parser = new RemoteParser();
} else {
  parser = new LocalParser();
}
String parsedDocument = parser.parse(document);
// ...
```
As you can see, the implementation of the parser depends on the length of the document. If we want to parse a long document, we will use the RemoteParser, otherwise, it will be the LocalParser. Let’s imagine the following situation: there is a huge amount of long documents to be parsed; the JIT compiler will notice that the actual type of parser is the RemoteParser. It will use the inline parse method (if applicable) and perform further optimizations assuming that the type of parser is always the RemoteParser.

Now, let’s call the code above with a huge amount of short documents. The assumption about the type of parser is no longer valid, so the optimizations applied before are also invalid. That means that the compiled methods have been made not entrant. JVM will discard all those optimizations and start compiling that code with LocalParser.

### JVM memory managment
Here you can see internal of java object and java class:

<a href="https://ibb.co/P9jQC2L"><img src="https://i.ibb.co/KLhwytP/image.png" alt="image" border="0"></a>

JVM have 3 types of memory storage:
1. Stack
2. Heap
3. [Metaspace](https://www.baeldung.com/java-permgen-metaspace#metaspace)

Metaspace - special storage, where JVM contains metadata (information about classes, methods, which methods compiled to native code and etc).
>Simply put, Metaspace is a new memory space – starting from the Java 8 version; it has replaced the older PermGen memory space.

### PermGen and Metaspace
__PermGen__ (Permanent Generation) - special place in JVM, used for metadata (java 7 and earlier versions):
<a href="https://imgbb.com/"><img src="https://i.ibb.co/5jfcvrc/image.png" alt="image" border="0"></a>

In PermGen, VM stores information about metadata, static primitive variables and static object references. Also, PermGen stores information about JIT compilation bytecode. Before java 7, [String pool](https://www.journaldev.com/797/what-is-java-string-pool) was also part of PermGen.

Due to its limited size, PermGen is the cause of the `java.lang.OutOfMemoryError: PermGen space error`. Simply put, class loaders are not handled properly by the garbage collector, resulting in memory leaks. Most often this happens when creating new bootloaders. This area was completely deleted in java 8.

__Metaspace__ - is a new memory area that appeared in version 8 of Java and replaced the obsolete PermGen. Their main difference lies in the way memory is allocated.

<a href="https://ibb.co/sq3k17f"><img src="https://i.ibb.co/cQTM8qB/image.png" alt="image" border="0"></a>

<a href="https://ibb.co/0qVJc7W"><img src="https://i.ibb.co/H7pgVSM/image.png" alt="image" border="0"></a>

Any objects from the heap, which referenced by the metaspace, will be never garbage collected.

As you can see, Metaspace size requirements depend both upon the number of classes loaded as well as the size of such class declarations. So it is easy to see the main cause for the `java.lang.OutOfMemoryError: Metaspace` is: __either too many classes or too big classes being loaded to the Metaspace__. By default, Metaspace grows automatically. However, here we have the [ability](https://docs.oracle.com/javase/9/gctuning/other-considerations.htm#JSGCT-GUID-B29C9153-3530-4C15-9154-E74F44E3DAD9) to manage memory:

Here you can set range of metaspace (from `MetaspaceSize` to `MaxMetaspaceSize`)
```
-XX:MaxMetaspaceSize=<metaspace size>[g|m|k]
-XX:MetaspaceSize=<metaspace size>[g|m|k]
```
The high-water mark is initially set to the value of the command-line option `-XX:MetaspaceSize`. It is raised or lowered based on the options `-XX:MaxMetaspaceFreeRatio` and `-XX:MinMetaspaceFreeRatio`. If the committed space available for class metadata as a percentage of the total committed space for class metadata is greater than `-XX:MaxMetaspaceFreeRatio`, then the high-water mark will be lowered. If it's less than `-XX:MinMetaspaceFreeRatio`, then the high-water mark will be raised.

<a href="https://ibb.co/LJ23Ff0"><img src="https://i.ibb.co/QC14gGJ/image.png" alt="image" border="0"></a>

Finally, lets see some example:

In this example the source code iterates over a loop and generates classes at the runtime. All those generated class definitions end up consuming Metaspace. Class generation complexity is taken care of by the javassist [library](https://en.wikipedia.org/wiki/Javassist).

The code will keep generating new classes and loading their definitions to Metaspace until the space is fully utilized and the `java.lang.OutOfMemoryError: Metaspace is thrown`.
```
static ClassPool classPool = ClassPool.getDefault();

public static void main(String[] args) throws Exception {
	for (int i = 0; ; i++) {
		classPool.makeClass("com.example.SomeClass" + i).toClass();
	}
}
```

Here we can detect metrics for metaspace (using VisualVM profiler):
1. With runtime class loading

<a href="https://ibb.co/GRyfytN"><img src="https://i.ibb.co/WfZNZPj/image.png" alt="image" border="0"></a>

2. Normal execution

<a href="https://ibb.co/98kdDyN"><img src="https://i.ibb.co/FxLdr8K/image.png" alt="image" border="0"></a>

__CONCLUSION__: As a result of the advent of Metaspace, the process of clearing memory has gained some advantages. Now the garbage collector automatically removes unnecessary classes from memory when the capacity allocated for storing metadata reaches its maximum value. Along with this, the probability of getting an `OutOfMemoryError` error has decreased.

Despite all the advantages, we still need to control and, if necessary, configure Metaspace to avoid memory leaks.

### JVM tuning
There are some VM [flags](https://docs.oracle.com/javase/8/docs/technotes/tools/unix/java.html) available for JVM tuning. Reference to Oracle tuning guide [here](https://docs.oracle.com/javase/8/docs/technotes/guides/vm/gctuning/index.html).

For JVM flags information use [next option](http://www.herongyang.com/Java-GC/Logging-Print-JVM-Options-PrintFlagsFinal.html):
```
-XX:+PrintFlagsFinal
```
>This option displays what options HotSpot ended up using for running Java code. 

__Change heap size:__
```
-XX:MaxHeapSize=size
```
>Sets the maximum size (in byes) of the memory allocation pool. This value must be a multiple of 1024 and greater than 2 MB. Append the letter k or K to indicate kilobytes, m or M to indicate megabytes, g or G to indicate gigabytes. The default value is chosen at runtime based on system configuration.

Also, by another flag you can change initial size of heap:
```
-XX:InitialHeapSize=size
```
>Sets the initial size (in bytes) of the memory allocation pool. This value must be either 0, or a multiple of 1024 and greater than 1 MB. Append the letter k or K to indicate kilobytes, m or M to indicate megabytes, g or G to indicate gigabytes. The default value is chosen at runtime based on system configuration.

Also, for server deployments, `-XX:InitialHeapSize` and `-XX:MaxHeapSize` are often set to the same value.

__JIT OPTIMIZATIONS:__

<a href="https://ibb.co/ydgHGgF"><img src="https://i.ibb.co/4K4h94R/image.png" alt="image" border="0"></a>

### CPU utilization
__CPU utilization__ - metric, which indicates _"non-idle"_ time for processor. 

Basicly, when we use [top](https://en.wikipedia.org/wiki/Top_(software)) utill for CPU usage, we can just notice that some thread/proccess consumes 90% of processor time (`%CPU` metric). 

But what if this thread/proccess really consumes less of CPU time?

We can assume that this task consumes 90%

<a href="https://ibb.co/8xFRTG1"><img src="https://i.ibb.co/hdpnhr3/image.png" alt="image" border="0"></a>

But in reallity it can be less

<a href="https://ibb.co/8gy522j"><img src="https://i.ibb.co/7t3WVVG/image.png" alt="image" border="0"></a>

__Stalled__ means, that CPU just not process instructions.

Another example from my Mackbook Pro activy monitor:

<a href="https://ibb.co/yVxBmyK"><img src="https://i.ibb.co/rvXfK6q/image.png" alt="image" border="0"></a>

### How to measure CPU usage correctly?
For a long time processor manufacturers were scaling their clockspeed quicker than DRAM was scaling its access latency (the "CPU DRAM gap"). That levelled out around 2005 with 3 GHz processors, and since then processors have scaled using more cores and hyperthreads, plus multi-socket configurations, all putting more demand on the memory subsystem. Processor manufacturers have tried to reduce this memory bottleneck with larger and smarter CPU caches, and faster memory busses and interconnects. But we're still [usually stalled](http://www.brendangregg.com/blog/2017-05-09/cpu-utilization-is-wrong.html).

### Lambda vs anon-classes
In many Java books you can find one thing, that anon-classes and lambdas are the same. 

Are this two code snippets equivalent?
```
 Runnable exec = new Runnable() {
        @Override
        public void run() {
            System.out.print("Hi from lambda");
        }
};

 Runnable exec = () -> {
        System.out.print("Hi from lambda");
};
```

In result, lambda and anon-class will give us the same result, but inside JVM it will be two different things. 

__So, lambdas is not a anon-classes!__

In java 7 was introduced new feature - [invokedynamic opcode](http://wiki.jvmlangsummit.com/images/1/1e/2011_Goetz_Lambda.pdf). This special "operator" works in runtime and use factory for instance creation:

<a href="https://ibb.co/PggvWRR"><img src="https://i.ibb.co/DQQHMNN/image.png" alt="image" border="0"></a>

First of all, `invokedynamic` will bootstrap creation (in first call) and then will use cached instance. So, in java 8 was introduced lambdas, which use `invokedynamic` feature.

Let's see how it can looks inside bytecode:
```
Runnable exec = () -> 
        System.out.print("Hi from lambda");

INVOKEDYNAMIC run()Ljava/lang/Runnable; [
      // handle kind 0x6 : INVOKESTATIC
      java/lang/invoke/LambdaMetafactory.metafactory(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/String;Ljava/lang/invoke/MethodType;Ljava/lang/invoke/MethodType;Ljava/lang/invoke/MethodHandle;Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/CallSite;
      // arguments:
      ()V, 
      // handle kind 0x6 : INVOKESTATIC
      test/Main.lambda$main$0()V, 
      ()V
```

We can see some part of bytecode, where dynamic invocation was happened and also we can see, that JVM use lambda factory for lambda creation (`LambdaMetafactory.metafactory`). 

This means, that when we will use lambda (instead of anon-class allocation) - it will reduce memory usage inside JVM, because of chaching. 
```
public static void main(String[] args) {
    allocateAnonClass();
    allocateAnonClass();

    allocateNonCaptureLambda();
    allocateNonCaptureLambda();
}

static void allocateNonCaptureLambda() {
    Runnable exec = () -> {
        System.out.print("Hi from lambda");
    };
    System.out.println(exec.hashCode());
}

static void allocateAnonClass() {
    var exec = new Runnable() {
        @Override
        public void run() {
            System.out.print("Hi from lambda");
        }
    };

    System.out.println(exec.hashCode());
}
```

So, in this case we will call allocation of anon-class and allocation of lambda twice. Let's see output:
```
For anon-class:
531885035
1418481495
For lambda:
135721597
135721597
```

Lambdas depends of method signature. How JVM can detect, that such lambda was already defined? Only if this lambda will be created in some method.

For example, if we will declare two excactly the same lambdas in such way:
```
 Runnable exec1 = () -> {
    System.out.print("Hi from lambda");
};

Runnable exec2 = () -> {
    System.out.print("Hi from lambda");
};
```
Then it will be two different instances, because JVM cannot understand, how to compare this two lambdas.

### Not all lambdas are cached
it's very important to use pure functions. Let's see the result with non-pure lambda, which use incoming method param:
```
void capture(int param) {
    Runnable exec = () -> {
        System.out.print("Hi from lambda with param=" + param);
    };
    System.out.println(exec.hashCode());
}

nonCapture(1);
nonCapture(1);
```

Or mutate scope:
```
void capture() {
    var localMutatedVariable = "Hahaha, not pure anymore!";
    Runnable exec = () -> {
        System.out.print("Hi from lambda, " + localMutatedVariable);
    };
    System.out.println(exec.hashCode());
}

nonCapture();
nonCapture();
```

It prints two different hashcodes, becouse each lambda use some local variable. Such functions/lambdas called capture (they capture for some incoming param). Pure functions can called non-capture functions.

So, for capturing lambdas new instance created everytime, for non-captured lambdas - cached factory used.
